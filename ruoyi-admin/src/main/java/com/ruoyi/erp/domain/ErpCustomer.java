package com.ruoyi.erp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户对象 erp_customer
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpCustomer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String customerNo;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String companyName;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contacts;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactsPhone;

    /** 联系地址 */
    @Excel(name = "联系地址")
    private String contactsAddress;

    /** 常用生产型号集 */
    @Excel(name = "常用生产型号集")
    private String modeNoList;

    /** 常用收货地址ID集 */
    @Excel(name = "常用收货地址ID集")
    private String consigneeIdList;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCustomerNo(String customerNo) 
    {
        this.customerNo = customerNo;
    }

    public String getCustomerNo() 
    {
        return customerNo;
    }
    public void setCompanyName(String companyName) 
    {
        this.companyName = companyName;
    }

    public String getCompanyName() 
    {
        return companyName;
    }
    public void setContacts(String contacts) 
    {
        this.contacts = contacts;
    }

    public String getContacts() 
    {
        return contacts;
    }
    public void setContactsPhone(String contactsPhone) 
    {
        this.contactsPhone = contactsPhone;
    }

    public String getContactsPhone() 
    {
        return contactsPhone;
    }
    public void setContactsAddress(String contactsAddress) 
    {
        this.contactsAddress = contactsAddress;
    }

    public String getContactsAddress() 
    {
        return contactsAddress;
    }
    public void setModeNoList(String modeNoList) 
    {
        this.modeNoList = modeNoList;
    }

    public String getModeNoList() 
    {
        return modeNoList;
    }
    public void setConsigneeIdList(String consigneeIdList) 
    {
        this.consigneeIdList = consigneeIdList;
    }

    public String getConsigneeIdList() 
    {
        return consigneeIdList;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerNo", getCustomerNo())
            .append("companyName", getCompanyName())
            .append("contacts", getContacts())
            .append("contactsPhone", getContactsPhone())
            .append("contactsAddress", getContactsAddress())
            .append("modeNoList", getModeNoList())
            .append("consigneeIdList", getConsigneeIdList())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
