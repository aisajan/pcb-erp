package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.erp.domain.ErpConsigneeInfo;
import com.ruoyi.erp.domain.ErpOrder;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpCustomer;
import com.ruoyi.erp.service.IErpCustomerService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 客户Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpCustomer")
public class ErpCustomerController extends BaseController
{
    private String prefix = "erp/erpCustomer";

    @Autowired
    private IErpCustomerService erpCustomerService;

    @RequiresPermissions("erp:erpCustomer:view")
    @GetMapping()
    public String erpCustomer()
    {
        return prefix + "/erpCustomer";
    }

    /**
     * 查询客户列表
     */
    @RequiresPermissions("erp:erpCustomer:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpCustomer erpCustomer)
    {
        startPage();
        List<ErpCustomer> list = erpCustomerService.selectErpCustomerList(erpCustomer);
        return getDataTable(list);
    }

    /**
     * 导出客户列表
     */
    @RequiresPermissions("erp:erpCustomer:export")
    @Log(title = "客户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpCustomer erpCustomer)
    {
        List<ErpCustomer> list = erpCustomerService.selectErpCustomerList(erpCustomer);
        ExcelUtil<ErpCustomer> util = new ExcelUtil<ErpCustomer>(ErpCustomer.class);
        return util.exportExcel(list, "erpCustomer");
    }

    /**
     * 新增客户
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存客户
     */
    @RequiresPermissions("erp:erpCustomer:add")
    @Log(title = "客户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpCustomer erpCustomer)
    {
        return toAjax(erpCustomerService.insertErpCustomer(erpCustomer));
    }

    /**
     * 修改客户
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpCustomer erpCustomer = erpCustomerService.selectErpCustomerById(id);
        mmap.put("erpCustomer", erpCustomer);
        return prefix + "/edit";
    }

    /**
     * 修改保存客户
     */
    @RequiresPermissions("erp:erpCustomer:edit")
    @Log(title = "客户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpCustomer erpCustomer)
    {
        return toAjax(erpCustomerService.updateErpCustomer(erpCustomer));
    }

    /**
     * 删除客户
     */
    @RequiresPermissions("erp:erpCustomer:remove")
    @Log(title = "客户", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpCustomerService.deleteErpCustomerByIds(ids));
    }

    @RequiresPermissions("erp:erpCustomer:view")
    @GetMapping("/pageSelectCustomer")
    public String pageSelectModel() {
        return prefix + "/pageSelectCustomer";
    }

    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("erp:erpCustomer:view")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<ErpCustomer> util = new ExcelUtil<ErpCustomer>(ErpCustomer.class);
        List<ErpCustomer> userList = util.importExcel(file.getInputStream());
        String operName = ShiroUtils.getSysUser().getLoginName();
        String message = erpCustomerService.importData(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    @RequiresPermissions("erp:erpCustomer:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate() {
        ExcelUtil<ErpCustomer> util = new ExcelUtil<ErpCustomer>(ErpCustomer.class);
        return util.importTemplateExcel("客户数据");
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpCustomer erpCustomer = erpCustomerService.selectErpCustomerById(id);
        mmap.put("erpCustomer", erpCustomer);
        return prefix + "/detail";
    }
}
