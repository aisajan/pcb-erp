package com.ruoyi.erp.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpModeAttributeRt;
import com.ruoyi.erp.service.IErpModeAttributeRtService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产型号拓展属性关联Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpModeAttributeRt")
public class ErpModeAttributeRtController extends BaseController
{
    private String prefix = "erp/erpModeAttributeRt";

    @Autowired
    private IErpModeAttributeRtService erpModeAttributeRtService;

    @RequiresPermissions("erp:erpModeAttributeRt:view")
    @GetMapping()
    public String erpModeAttributeRt()
    {
        return prefix + "/erpModeAttributeRt";
    }

    /**
     * 查询生产型号拓展属性关联列表
     */
    @RequiresPermissions("erp:erpModeAttributeRt:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpModeAttributeRt erpModeAttributeRt)
    {
        startPage();
        List<ErpModeAttributeRt> list = erpModeAttributeRtService.selectErpModeAttributeRtList(erpModeAttributeRt);
        return getDataTable(list);
    }

    /**
     * 导出生产型号拓展属性关联列表
     */
    @RequiresPermissions("erp:erpModeAttributeRt:export")
    @Log(title = "生产型号拓展属性关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpModeAttributeRt erpModeAttributeRt)
    {
        List<ErpModeAttributeRt> list = erpModeAttributeRtService.selectErpModeAttributeRtList(erpModeAttributeRt);
        ExcelUtil<ErpModeAttributeRt> util = new ExcelUtil<ErpModeAttributeRt>(ErpModeAttributeRt.class);
        return util.exportExcel(list, "erpModeAttributeRt");
    }

    /**
     * 新增生产型号拓展属性关联
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存生产型号拓展属性关联
     */
    @RequiresPermissions("erp:erpModeAttributeRt:add")
    @Log(title = "生产型号拓展属性关联", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpModeAttributeRt erpModeAttributeRt)
    {
        return toAjax(erpModeAttributeRtService.insertErpModeAttributeRt(erpModeAttributeRt));
    }

    /**
     * 修改生产型号拓展属性关联
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpModeAttributeRt erpModeAttributeRt = erpModeAttributeRtService.selectErpModeAttributeRtById(id);
        mmap.put("erpModeAttributeRt", erpModeAttributeRt);
        return prefix + "/edit";
    }

    /**
     * 修改保存生产型号拓展属性关联
     */
    @RequiresPermissions("erp:erpModeAttributeRt:edit")
    @Log(title = "生产型号拓展属性关联", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpModeAttributeRt erpModeAttributeRt)
    {
        return toAjax(erpModeAttributeRtService.updateErpModeAttributeRt(erpModeAttributeRt));
    }

    /**
     * 删除生产型号拓展属性关联
     */
    @RequiresPermissions("erp:erpModeAttributeRt:remove")
    @Log(title = "生产型号拓展属性关联", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpModeAttributeRtService.deleteErpModeAttributeRtByIds(ids));
    }
}
