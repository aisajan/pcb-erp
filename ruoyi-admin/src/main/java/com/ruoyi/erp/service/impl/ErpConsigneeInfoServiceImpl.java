package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpConsigneeInfoMapper;
import com.ruoyi.erp.domain.ErpConsigneeInfo;
import com.ruoyi.erp.service.IErpConsigneeInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 常用收货地址Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpConsigneeInfoServiceImpl implements IErpConsigneeInfoService 
{
    @Autowired
    private ErpConsigneeInfoMapper erpConsigneeInfoMapper;

    /**
     * 查询常用收货地址
     * 
     * @param id 常用收货地址ID
     * @return 常用收货地址
     */
    @Override
    public ErpConsigneeInfo selectErpConsigneeInfoById(String id)
    {
        return erpConsigneeInfoMapper.selectErpConsigneeInfoById(id);
    }

    /**
     * 查询常用收货地址列表
     * 
     * @param erpConsigneeInfo 常用收货地址
     * @return 常用收货地址
     */
    @Override
    public List<ErpConsigneeInfo> selectErpConsigneeInfoList(ErpConsigneeInfo erpConsigneeInfo)
    {
        return erpConsigneeInfoMapper.selectErpConsigneeInfoList(erpConsigneeInfo);
    }

    /**
     * 新增常用收货地址
     * 
     * @param erpConsigneeInfo 常用收货地址
     * @return 结果
     */
    @Override
    public int insertErpConsigneeInfo(ErpConsigneeInfo erpConsigneeInfo)
    {
        erpConsigneeInfo.setId(IdUtils.fastSimpleUUID());
        erpConsigneeInfo.setCreateTime(DateUtils.getNowDate());
        return erpConsigneeInfoMapper.insertErpConsigneeInfo(erpConsigneeInfo);
    }

    /**
     * 修改常用收货地址
     * 
     * @param erpConsigneeInfo 常用收货地址
     * @return 结果
     */
    @Override
    public int updateErpConsigneeInfo(ErpConsigneeInfo erpConsigneeInfo)
    {
        erpConsigneeInfo.setUpdateTime(DateUtils.getNowDate());
        return erpConsigneeInfoMapper.updateErpConsigneeInfo(erpConsigneeInfo);
    }

    /**
     * 删除常用收货地址对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpConsigneeInfoByIds(String ids)
    {
        return erpConsigneeInfoMapper.deleteErpConsigneeInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除常用收货地址信息
     * 
     * @param id 常用收货地址ID
     * @return 结果
     */
    @Override
    public int deleteErpConsigneeInfoById(String id)
    {
        return erpConsigneeInfoMapper.deleteErpConsigneeInfoById(id);
    }
}
