package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpWorkProcessLogMapper;
import com.ruoyi.erp.domain.ErpWorkProcessLog;
import com.ruoyi.erp.service.IErpWorkProcessLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 生产进度日志Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpWorkProcessLogServiceImpl implements IErpWorkProcessLogService 
{
    @Autowired
    private ErpWorkProcessLogMapper erpWorkProcessLogMapper;

    /**
     * 查询生产进度日志
     * 
     * @param id 生产进度日志ID
     * @return 生产进度日志
     */
    @Override
    public ErpWorkProcessLog selectErpWorkProcessLogById(String id)
    {
        return erpWorkProcessLogMapper.selectErpWorkProcessLogById(id);
    }

    /**
     * 查询生产进度日志列表
     * 
     * @param erpWorkProcessLog 生产进度日志
     * @return 生产进度日志
     */
    @Override
    public List<ErpWorkProcessLog> selectErpWorkProcessLogList(ErpWorkProcessLog erpWorkProcessLog)
    {
        return erpWorkProcessLogMapper.selectErpWorkProcessLogList(erpWorkProcessLog);
    }

    /**
     * 新增生产进度日志
     * 
     * @param erpWorkProcessLog 生产进度日志
     * @return 结果
     */
    @Override
    public int insertErpWorkProcessLog(ErpWorkProcessLog erpWorkProcessLog)
    {
        erpWorkProcessLog.setId(IdUtils.fastSimpleUUID());
        erpWorkProcessLog.setCreateTime(DateUtils.getNowDate());
        return erpWorkProcessLogMapper.insertErpWorkProcessLog(erpWorkProcessLog);
    }

    /**
     * 修改生产进度日志
     * 
     * @param erpWorkProcessLog 生产进度日志
     * @return 结果
     */
    @Override
    public int updateErpWorkProcessLog(ErpWorkProcessLog erpWorkProcessLog)
    {
        erpWorkProcessLog.setUpdateTime(DateUtils.getNowDate());
        return erpWorkProcessLogMapper.updateErpWorkProcessLog(erpWorkProcessLog);
    }

    /**
     * 删除生产进度日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpWorkProcessLogByIds(String ids)
    {
        return erpWorkProcessLogMapper.deleteErpWorkProcessLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除生产进度日志信息
     * 
     * @param id 生产进度日志ID
     * @return 结果
     */
    @Override
    public int deleteErpWorkProcessLogById(String id)
    {
        return erpWorkProcessLogMapper.deleteErpWorkProcessLogById(id);
    }
}
